namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DaveChange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "NameSurname", c => c.String());
            AddColumn("dbo.Customers", "PhoneNo", c => c.String());
            AddColumn("dbo.Customers", "CarBrand", c => c.String());
            AddColumn("dbo.Customers", "CarModel", c => c.String());
            AddColumn("dbo.Customers", "Damage_no", c => c.String());
            AddColumn("dbo.Customers", "Registration_no", c => c.String());
            AddColumn("dbo.Customers", "Prod_no", c => c.String());
            AddColumn("dbo.Customers", "TU", c => c.String());
            AddColumn("dbo.Customers", "Zgloszenie_szkody_w_TU", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Ogledziny_przednapr_w_TU", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Dostar_do_serw_koszt_napr", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Wykon_kalk_serw", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Zatw_kalk_przez_TU", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Rozp_napr", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Zgl_TU_dod_ogl", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Ogl_dod_wyk_TU", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Zatw_koszt_ogl_dod", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Zamow_czesci", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Odb_czesci", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Zakon_napr", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Odbior_poj", c => c.DateTime(nullable: false));
            DropColumn("dbo.Customers", "Name");
            DropColumn("dbo.Customers", "Surname");
            DropColumn("dbo.Customers", "Doc_no");
            DropColumn("dbo.Customers", "Address");
            DropColumn("dbo.Customers", "Order_no");
            DropColumn("dbo.Customers", "Price");
            DropColumn("dbo.Customers", "Start_date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Start_date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "Price", c => c.String(maxLength: 4000));
            AddColumn("dbo.Customers", "Order_no", c => c.String(maxLength: 4000));
            AddColumn("dbo.Customers", "Address", c => c.String(maxLength: 4000));
            AddColumn("dbo.Customers", "Doc_no", c => c.String(maxLength: 4000));
            AddColumn("dbo.Customers", "Surname", c => c.String(maxLength: 4000));
            AddColumn("dbo.Customers", "Name", c => c.String(maxLength: 4000));
            DropColumn("dbo.Customers", "Odbior_poj");
            DropColumn("dbo.Customers", "Zakon_napr");
            DropColumn("dbo.Customers", "Odb_czesci");
            DropColumn("dbo.Customers", "Zamow_czesci");
            DropColumn("dbo.Customers", "Zatw_koszt_ogl_dod");
            DropColumn("dbo.Customers", "Ogl_dod_wyk_TU");
            DropColumn("dbo.Customers", "Zgl_TU_dod_ogl");
            DropColumn("dbo.Customers", "Rozp_napr");
            DropColumn("dbo.Customers", "Zatw_kalk_przez_TU");
            DropColumn("dbo.Customers", "Wykon_kalk_serw");
            DropColumn("dbo.Customers", "Dostar_do_serw_koszt_napr");
            DropColumn("dbo.Customers", "Ogledziny_przednapr_w_TU");
            DropColumn("dbo.Customers", "Zgloszenie_szkody_w_TU");
            DropColumn("dbo.Customers", "TU");
            DropColumn("dbo.Customers", "Prod_no");
            DropColumn("dbo.Customers", "Registration_no");
            DropColumn("dbo.Customers", "Damage_no");
            DropColumn("dbo.Customers", "CarModel");
            DropColumn("dbo.Customers", "CarBrand");
            DropColumn("dbo.Customers", "PhoneNo");
            DropColumn("dbo.Customers", "NameSurname");
        }
    }
}
