namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDave_v2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "Get_date", c => c.String());
            AlterColumn("dbo.Customers", "Zgloszenie_szkody_w_TU", c => c.String());
            AlterColumn("dbo.Customers", "Ogledziny_przednapr_w_TU", c => c.String());
            AlterColumn("dbo.Customers", "Dostar_do_serw_koszt_napr", c => c.String());
            AlterColumn("dbo.Customers", "Wykon_kalk_serw", c => c.String());
            AlterColumn("dbo.Customers", "Zatw_kalk_przez_TU", c => c.String());
            AlterColumn("dbo.Customers", "Rozp_napr", c => c.String());
            AlterColumn("dbo.Customers", "Zgl_TU_dod_ogl", c => c.String());
            AlterColumn("dbo.Customers", "Ogl_dod_wyk_TU", c => c.String());
            AlterColumn("dbo.Customers", "Zatw_koszt_ogl_dod", c => c.String());
            AlterColumn("dbo.Customers", "Zamow_czesci", c => c.String());
            AlterColumn("dbo.Customers", "Odb_czesci", c => c.String());
            AlterColumn("dbo.Customers", "Zakon_napr", c => c.String());
            AlterColumn("dbo.Customers", "Odbior_poj", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "Odbior_poj", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zakon_napr", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Odb_czesci", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zamow_czesci", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zatw_koszt_ogl_dod", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Ogl_dod_wyk_TU", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zgl_TU_dod_ogl", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Rozp_napr", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zatw_kalk_przez_TU", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Wykon_kalk_serw", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Dostar_do_serw_koszt_napr", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Ogledziny_przednapr_w_TU", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Zgloszenie_szkody_w_TU", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Customers", "Get_date", c => c.DateTime(nullable: false));
        }
    }
}
