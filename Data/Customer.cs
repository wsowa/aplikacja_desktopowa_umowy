﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    
    public class Customer
    {

        public int Id { get; set; }
        public string NameSurname { get; set; }
        public string PhoneNo { get; set; }
        public string CarBrand { get; set; }
        public string CarModel { get; set; }
        public string Damage_no { get; set; }
        public string Registration_no { get; set; }
        public string Prod_no { get; set; }
        public string TU { get; set; }
        public string Get_date { get; set; }
        public string Zgloszenie_szkody_w_TU { get; set; }
        public string Ogledziny_przednapr_w_TU { get; set; }
        public string Dostar_do_serw_koszt_napr { get; set; }
        public string Wykon_kalk_serw { get; set; }
        public string Zatw_kalk_przez_TU { get; set; }
        public string Rozp_napr { get; set; }
        public string Zgl_TU_dod_ogl { get; set; }
        public string Ogl_dod_wyk_TU { get; set; }
        public string Zatw_koszt_ogl_dod { get; set; }
        public string Zamow_czesci { get; set; }
        public string Odb_czesci { get; set; }
        public string Zakon_napr { get; set; }
        public string Odbior_poj { get; set; }
        public string Description { get; set; }
    }
}
