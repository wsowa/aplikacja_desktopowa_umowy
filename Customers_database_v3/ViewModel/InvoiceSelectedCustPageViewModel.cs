﻿using Data;
using Customers_database_v3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Customers_database_v3.ViewModel
{
    class InvoiceSelectedCustPageViewModel : DependencyObject
    {
        CustomerDbContext db;

        public Customer CustomerS
        {
            get { return (Customer)GetValue(CustomerProperty); }
            set { SetValue(CustomerProperty, value); }
        }
        public static readonly DependencyProperty CustomerProperty =
            DependencyProperty.Register("Customer", typeof(Customer), typeof(InvoiceSelectedCustPageViewModel), new PropertyMetadata(null));

        public InvoiceSelectedCustPageViewModel(Customer customer)
        {
            db = new CustomerDbContext();
            CustomerS = customer;
            CustomerS.NameSurname = customer.NameSurname;
            db.Dispose();
        }
    }
}
