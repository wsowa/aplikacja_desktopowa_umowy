﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.ObjectModel;
using Customers_database_v3.Model;
using Data;
using System.Windows.Input;
using Customers_database_v3.Menu;

namespace Customers_database_v3.ViewModel
{
    class ShowCustomersPageViewModel : DependencyObject
    {
        CustomerDbContext db;
        private enum UpdatingData { Customer };

        public ObservableCollection<Customer> Customers
        {
            get { return (ObservableCollection<Customer>)GetValue(CustomersProperty); }
            set { SetValue(CustomersProperty, value); }
        }
        public static readonly DependencyProperty CustomersProperty =
            DependencyProperty.Register("Customers", typeof(ObservableCollection<Customer>), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public Customer SelectedCustomer
        {
            get { return (Customer)GetValue(SelectedCustomerProperty); }
            set { SetValue(SelectedCustomerProperty, value); }
        }
        public static readonly DependencyProperty SelectedCustomerProperty =
            DependencyProperty.Register("SelectedCustomer", typeof(Customer), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ICommand AddCustomer
        {
            get { return (ICommand)GetValue(AddCustomerProperty); }
            set { SetValue(AddCustomerProperty, value); }
        }
        public static readonly DependencyProperty AddCustomerProperty =
            DependencyProperty.Register("AddCustomer", typeof(ICommand), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ICommand ShowCustomer
        {
            get { return (ICommand)GetValue(ShowCustomerProperty); }
            set { SetValue(ShowCustomerProperty, value); }
        }
        public static readonly DependencyProperty ShowCustomerProperty =
            DependencyProperty.Register("ShowCustomer", typeof(ICommand), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ICommand EditCustomer
        {
            get { return (ICommand)GetValue(EditCustomerProperty); }
            set { SetValue(EditCustomerProperty, value); }
        }
        public static readonly DependencyProperty EditCustomerProperty =
            DependencyProperty.Register("EditCustomer", typeof(ICommand), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ICommand InvoiceSelectedCust
        {
            get { return (ICommand)GetValue(InvoiceSelectedCustProperty); }
            set { SetValue(InvoiceSelectedCustProperty, value); }
        }
        public static readonly DependencyProperty InvoiceSelectedCustProperty =
            DependencyProperty.Register("InvoiceSelectedCust", typeof(ICommand), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ICommand DeleteCustomer
        {
            get { return (ICommand)GetValue(DeleteCustomerProperty); }
            set { SetValue(DeleteCustomerProperty, value); }
        }
        public static readonly DependencyProperty DeleteCustomerProperty =
            DependencyProperty.Register("DeleteCustomer", typeof(ICommand), typeof(ShowCustomersPageViewModel), new PropertyMetadata(null));

        public ShowCustomersPageViewModel()
        {
            db = new CustomerDbContext();
            Customers = new ObservableCollection<Customer>(db.Customers);

            ShowCustomer = new Command(ShowCustomerPageViewModel);
            AddCustomer = new Command(AddNewCustomerPageViewModel);
            EditCustomer = new Command(EditCustomerPageViewModel);
            DeleteCustomer = new Command(deleteCustomer);
            InvoiceSelectedCust = new Command(InvoiceSelectedCustPageViewModel);
            db.Dispose();
        }

        public void InvoiceSelectedCustPageViewModel()
        {
            db = new CustomerDbContext();
            if (SelectedCustomer == null)
            {
                MessageBox.Show("Nie wybrano klienta");
                return;
            }
            Customer Invcustomer = SelectedCustomer;
            InvoiceSelectedCustPage customerWindow = new InvoiceSelectedCustPage();
            InvoiceSelectedCustPageViewModel InvCustViewModel = new InvoiceSelectedCustPageViewModel(Invcustomer);
            customerWindow.DataContext = InvCustViewModel;
            //customerWindow.Show();
            Switcher.Switch(customerWindow);
        }

        public void ShowCustomerPageViewModel()
        {
            db = new CustomerDbContext();
            if (SelectedCustomer == null)
            {
                MessageBox.Show("Nie wybrano klienta");
                return;
            }
            Customer customer = SelectedCustomer;
            ShowCustomerPage customerWindow = new ShowCustomerPage();
            ShowCustomerPageViewModel customerViewModel = new ShowCustomerPageViewModel(customer);
            customerWindow.DataContext = customerViewModel;
            //customerWindow.Show();
            Switcher.Switch(customerWindow);
        }

        private void AddNewCustomerPageViewModel()
        {
            db = new CustomerDbContext();
            Customer newCustomer = new Customer();
            AddPage addCustomerWindow = new AddPage();
            AddCustomerPageViewModel addCustomerViewModel = new AddCustomerPageViewModel(newCustomer);
            addCustomerWindow.DataContext = addCustomerViewModel;

            addCustomerViewModel.OnDataFilled += (closeResult) =>
            {
                if (closeResult == true)
                {
                    try
                    {
                        db.Customers.Add(newCustomer);
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
              //  addCustomerWindow.Close();
                updateDataGrid(UpdatingData.Customer);
            };

            Switcher.Switch(addCustomerWindow);
            
        }

        private void EditCustomerPageViewModel()
        {
            db = new CustomerDbContext();
            if (SelectedCustomer == null)
            {
                MessageBox.Show("Nie wybrano klienta");
                return;
            }
            Customer editingCustomer = SelectedCustomer;
            EditPage customerWindow = new EditPage();
            EditCustomerPageViewModel customerViewModel = new EditCustomerPageViewModel(editingCustomer);
            customerWindow.DataContext = customerViewModel;

            customerViewModel.OnDataFilled += (closeResult) =>
            {
                if (closeResult == true)
                {
                    try
                    {
                        db.Entry<Customer>(editingCustomer).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                    }
                }
                 Switcher.Switch(new ShowCustomersPage());
                updateDataGrid(UpdatingData.Customer);
            };
            Switcher.Switch(customerWindow);
        }

        private void deleteCustomer()
        {
            db = new CustomerDbContext();
            if (SelectedCustomer != null)
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Na pewno chcesz usunąć klienta?", "Usuwanie", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    Customer delitingCustomer = db.Customers.Where(o => o.Id == SelectedCustomer.Id).First();
                    db.Customers.Remove(delitingCustomer);
                    db.SaveChanges();
                    updateDataGrid(UpdatingData.Customer);
                }
            }
            else
            {
                MessageBox.Show("Zaznacz klienta");
                return;
            }
        }

        private async void updateDataGrid(UpdatingData data)
        {
            try
            {
                switch (data)
                {
                    case UpdatingData.Customer:
                        Customers = new ObservableCollection<Customer>(await Task.Run(() => db.Customers.ToList()));
                        break;
                   
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

    }
}
