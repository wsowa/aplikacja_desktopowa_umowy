﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Customers_database_v3.ViewModel
{
    class ShowCustomerPageViewModel : DependencyObject
    {
        CustomerDbContext db;
        public Customer Customert
        {
            get { return (Customer)GetValue(CustomerProperty); }
            set { SetValue(CustomerProperty, value); }
        }
        public static readonly DependencyProperty CustomerProperty =
            DependencyProperty.Register("Customer", typeof(Customer), typeof(ShowCustomerPageViewModel), new PropertyMetadata(null));

        //public event Action<bool> OnDataFilled;


        public ShowCustomerPageViewModel(Customer customer)
        {
            db = new CustomerDbContext();
            Customert = customer;
            Customert.NameSurname = customer.NameSurname;
            db.Dispose();
        }

    }
}
