﻿using Customers_database_v3.Model;
using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Customers_database_v3.ViewModel
{
    class AddCustomerPageViewModel : DependencyObject
    {
        CustomerDbContext db;

        public AddCustomerPageViewModel(Customer customer)
        {
            NewCustomer = customer;
            AddNewCustomer = new Command(addCustomer);
        }

        #region Dependency Property
        public string NewCustomerNameSurname
        {
            get { return (string)GetValue(NewCustomerNameSurnameProperty); }
            set { SetValue(NewCustomerNameSurnameProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerNameSurnameProperty =
            DependencyProperty.Register("NewCustomerNameSurname", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));


        public string NewCustomerPhoneNo
        {
            get { return (string)GetValue(NewCustomerPhoneNoProperty); }
            set { SetValue(NewCustomerPhoneNoProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerPhoneNoProperty =
            DependencyProperty.Register("NewCustomerPhoneNo", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerCarBrand
        {
            get { return (string)GetValue(NewCustomerCarBrandProperty); }
            set { SetValue(NewCustomerCarBrandProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerCarBrandProperty =
            DependencyProperty.Register("NewCustomerCarBrand", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerCarModel
        {
            get { return (string)GetValue(NewCustomerCarModelProperty); }
            set { SetValue(NewCustomerCarModelProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerCarModelProperty =
            DependencyProperty.Register("NewCustomerCarModel", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDamage_no
        {
            get { return (string)GetValue(NewCustomerDamage_noProperty); }
            set { SetValue(NewCustomerDamage_noProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDamage_noProperty =
            DependencyProperty.Register("NewCustomerDamage_no", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerRegistration_no
        {
            get { return (string)GetValue(NewCustomerRegistration_noProperty); }
            set { SetValue(NewCustomerRegistration_noProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerRegistration_noProperty =
            DependencyProperty.Register("NewCustomerRegistration_no", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerProd_no
        {
            get { return (string)GetValue(NewCustomerProd_noProperty); }
            set { SetValue(NewCustomerProd_noProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerProd_noProperty =
            DependencyProperty.Register("NewCustomerProd_no", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerTU
        {
            get { return (string)GetValue(NewCustomerTUProperty); }
            set { SetValue(NewCustomerTUProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerTUProperty =
            DependencyProperty.Register("NewCustomerTU", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerGetDate
        {
            get { return (string)GetValue(NewCustomerGetDateProperty); }
            set { SetValue(NewCustomerGetDateProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerGetDateProperty =
            DependencyProperty.Register("NewCustomerGetDate", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZgloszenie_szkody_w_TU
        //{
        //    get { return (string)GetValue(NewCustomerZgloszenie_szkody_w_TUProperty); }
        //    set { SetValue(NewCustomerZgloszenie_szkody_w_TUProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZgloszenie_szkody_w_TUProperty =
        //    DependencyProperty.Register("NewCustomerZgloszenie_szkody_w_TU", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerOgledziny_przednapr_w_TU
        //{
        //    get { return (string)GetValue(NewCustomerOgledziny_przednapr_w_TUProperty); }
        //    set { SetValue(NewCustomerOgledziny_przednapr_w_TUProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerOgledziny_przednapr_w_TUProperty =
        //    DependencyProperty.Register("NewCustomerOgledziny_przednapr_w_TU", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerDostar_do_serw_koszt_napr
        //{
        //    get { return (string)GetValue(NewCustomerDostar_do_serw_koszt_naprProperty); }
        //    set { SetValue(NewCustomerDostar_do_serw_koszt_naprProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerDostar_do_serw_koszt_naprProperty =
        //    DependencyProperty.Register("NewCustomerDostar_do_serw_koszt_napr", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerWykon_kalk_serw
        //{
        //    get { return (string)GetValue(NewCustomerWykon_kalk_serwProperty); }
        //    set { SetValue(NewCustomerWykon_kalk_serwProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerWykon_kalk_serwProperty =
        //    DependencyProperty.Register("NewCustomerWykon_kalk_serw", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZatw_kalk_przez_TU
        //{
        //    get { return (string)GetValue(NewCustomerZatw_kalk_przez_TUProperty); }
        //    set { SetValue(NewCustomerZatw_kalk_przez_TUProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZatw_kalk_przez_TUProperty =
        //    DependencyProperty.Register("NewCustomerZatw_kalk_przez_TU", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerRozp_napr
        //{
        //    get { return (string)GetValue(NewCustomerRozp_naprProperty); }
        //    set { SetValue(NewCustomerRozp_naprProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerRozp_naprProperty =
        //    DependencyProperty.Register("NewCustomerRozp_napr", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZgl_TU_dod_ogl
        //{
        //    get { return (string)GetValue(NewCustomerZgl_TU_dod_oglProperty); }
        //    set { SetValue(NewCustomerZgl_TU_dod_oglProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZgl_TU_dod_oglProperty =
        //    DependencyProperty.Register("NewCustomerZgl_TU_dod_ogl", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerOgl_dod_wyk_TU
        //{
        //    get { return (string)GetValue(NewCustomerOgl_dod_wyk_TUProperty); }
        //    set { SetValue(NewCustomerOgl_dod_wyk_TUProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerOgl_dod_wyk_TUProperty =
        //    DependencyProperty.Register("NewCustomerOgl_dod_wyk_TU", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZatw_koszt_ogl_dod
        //{
        //    get { return (string)GetValue(NewCustomerZatw_koszt_ogl_dodProperty); }
        //    set { SetValue(NewCustomerZatw_koszt_ogl_dodProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZatw_koszt_ogl_dodProperty =
        //    DependencyProperty.Register("NewCustomerZatw_koszt_ogl_dod", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZamow_czesci
        //{
        //    get { return (string)GetValue(NewCustomerZamow_czesciProperty); }
        //    set { SetValue(NewCustomerZamow_czesciProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZamow_czesciProperty =
        //    DependencyProperty.Register("NewCustomerZamow_czesci", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerOdb_czesci
        //{
        //    get { return (string)GetValue(NewCustomerOdb_czesciProperty); }
        //    set { SetValue(NewCustomerOdb_czesciProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerOdb_czesciProperty =
        //    DependencyProperty.Register("NewCustomerOdb_czesci", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerZakon_napr
        //{
        //    get { return (string)GetValue(NewCustomerZakon_naprProperty); }
        //    set { SetValue(NewCustomerZakon_naprProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerZakon_naprProperty =
        //    DependencyProperty.Register("NewCustomerZakon_napr", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        //public string NewCustomerOdbior_poj
        //{
        //    get { return (string)GetValue(NewCustomerOdbior_pojProperty); }
        //    set { SetValue(NewCustomerOdbior_pojProperty, value); }
        //}
        //public static readonly DependencyProperty NewCustomerOdbior_pojProperty =
        //    DependencyProperty.Register("NewCustomerOdbior_poj", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public string NewCustomerDescription
        {
            get { return (string)GetValue(NewCustomerDescriptionProperty); }
            set { SetValue(NewCustomerDescriptionProperty, value); }
        }
        public static readonly DependencyProperty NewCustomerDescriptionProperty =
            DependencyProperty.Register("NewCustomerDescription", typeof(string), typeof(AddCustomerPageViewModel), new PropertyMetadata(""));

        public ICommand AddNewCustomer
        {
            get { return (ICommand)GetValue(AddNewCustomerProperty); }
            set { SetValue(AddNewCustomerProperty, value); }
        }
        public static readonly DependencyProperty AddNewCustomerProperty =
            DependencyProperty.Register("AddNewCustomer", typeof(ICommand), typeof(AddCustomerPageViewModel), new PropertyMetadata(null));
        #endregion

        private Customer NewCustomer { set; get; }

        public event Action<bool> OnDataFilled;

        private void addCustomer()
        {
            if (OnDataFilled != null)
            {
                if (string.IsNullOrEmpty(NewCustomerNameSurname) || string.IsNullOrWhiteSpace(NewCustomerNameSurname))
                {
                    MessageBox.Show("Pole imię i nazwisko nie może być puste!");
                    return;
                }
                if (NewCustomerNameSurname.Any(c => char.IsDigit(c)))
                {
                    MessageBox.Show("Pole imię i nazwisko nie może zawierac cyfr!");
                    return;
                }

                if (string.IsNullOrEmpty(NewCustomerPhoneNo) || string.IsNullOrWhiteSpace(NewCustomerPhoneNo))
                {
                    MessageBox.Show("Pole numer telefonu nie może być puste!");
                    return;
                }
               
                if (string.IsNullOrEmpty(NewCustomerCarBrand) || string.IsNullOrWhiteSpace(NewCustomerCarBrand))
                {
                    MessageBox.Show("Pole marka samochodu nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerCarModel) || string.IsNullOrWhiteSpace(NewCustomerCarModel))
                {
                    MessageBox.Show("Pole adres nie może być puste!");
                    return;
                }
                if (string.IsNullOrEmpty(NewCustomerDamage_no) || string.IsNullOrWhiteSpace(NewCustomerDamage_no))
                {
                    MessageBox.Show("Pole nr szkody nie może być puste!");
                    return;
                }
                
                if (string.IsNullOrEmpty(NewCustomerRegistration_no) || string.IsNullOrWhiteSpace(NewCustomerRegistration_no))
                {
                    MessageBox.Show("Pole numer rejestracji nie może być puste!");
                    return;
                }




                NewCustomer.NameSurname = NewCustomerNameSurname;
                NewCustomer.PhoneNo = NewCustomerPhoneNo;
                NewCustomer.CarBrand = NewCustomerCarBrand;
                NewCustomer.CarModel = NewCustomerCarModel;
                NewCustomer.Damage_no = NewCustomerDamage_no;
                NewCustomer.Registration_no = NewCustomerRegistration_no;
                NewCustomer.Prod_no = NewCustomerProd_no;
                NewCustomer.TU = NewCustomerTU;
                NewCustomer.Get_date = NewCustomerGetDate;
                //NewCustomer.Zgloszenie_szkody_w_TU = Convert.ToDateTime(NewCustomerZgloszenie_szkody_w_TU);
                //NewCustomer.Ogledziny_przednapr_w_TU = Convert.ToDateTime(NewCustomerOgledziny_przednapr_w_TU);
                //NewCustomer.Dostar_do_serw_koszt_napr = Convert.ToDateTime(NewCustomerDostar_do_serw_koszt_napr);
               
                //NewCustomer.Wykon_kalk_serw = Convert.ToDateTime(NewCustomerWykon_kalk_serw);
                //NewCustomer.Zatw_kalk_przez_TU = Convert.ToDateTime(NewCustomerZatw_kalk_przez_TU);
                //NewCustomer.Rozp_napr = Convert.ToDateTime(NewCustomerRozp_napr);
                //NewCustomer.Zgl_TU_dod_ogl = Convert.ToDateTime(NewCustomerZgl_TU_dod_ogl);
                //NewCustomer.Ogl_dod_wyk_TU = Convert.ToDateTime(NewCustomerOgl_dod_wyk_TU);
                //NewCustomer.Zatw_koszt_ogl_dod = Convert.ToDateTime(NewCustomerZatw_koszt_ogl_dod);
                //NewCustomer.Zamow_czesci = Convert.ToDateTime(NewCustomerZamow_czesci);
                //NewCustomer.Odb_czesci = Convert.ToDateTime(NewCustomerOdb_czesci);
                //NewCustomer.Zakon_napr = Convert.ToDateTime(NewCustomerZakon_napr);
                //NewCustomer.Odbior_poj = Convert.ToDateTime(NewCustomerOdbior_poj);
                NewCustomer.Description = NewCustomerDescription;

                OnDataFilled(true);
                MessageBox.Show("Dodano Klienta!");
               

            }
        }

    }
}

