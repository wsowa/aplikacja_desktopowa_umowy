﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for InvoiceSelectedCustPage.xaml
    /// </summary>
    public partial class InvoiceSelectedCustPage : UserControl
    {
        public InvoiceSelectedCustPage()
        {
            InitializeComponent();
        }
        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }
        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Protokół naprawy ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 80;
            document.Add(prgHeader);

            Paragraph pieczatka = new Paragraph();
            pieczatka.Alignment = Element.ALIGN_LEFT;
            pieczatka.Add(new Chunk("pieczęć serwisu ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            
            document.Add(pieczatka);

            PdfPTable dane = new PdfPTable(2);

            dane.TotalWidth = 500f;
            
            dane.LockedWidth = true;
            float[] widths1 = new float[] { 300f, 200f };
            dane.SetWidths(widths1);
            dane.HorizontalAlignment = 0;
            dane.SpacingBefore = 20f;
            dane.SpacingAfter = 30f;

            PdfPCell wlasciciel = new PdfPCell(new Phrase("Właściciel uszkodzonego pojazdu: " + ImieNazwisko.Content, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            wlasciciel.HorizontalAlignment = Element.ALIGN_LEFT;
            wlasciciel.Border = 0;
            dane.AddCell(wlasciciel);
            PdfPCell tel = new PdfPCell(new Phrase("tel: " + NrTel.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tel.HorizontalAlignment = Element.ALIGN_LEFT;
            tel.Border = 0;
            dane.AddCell(tel);
            
            PdfPCell marka = new PdfPCell(new Phrase("Marka pojazdu: " + MarkaSamochodu.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            marka.HorizontalAlignment = Element.ALIGN_LEFT;
            marka.Border = 0;
            dane.AddCell(marka);
            PdfPCell model = new PdfPCell(new Phrase("Model: " + ModelSamochodu.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            model.HorizontalAlignment = Element.ALIGN_LEFT;
            model.Border = 0;
            dane.AddCell(model);
           
            PdfPCell rej = new PdfPCell(new Phrase("Nr rejestracyjny pojazdu: " + NumerRej.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            rej.HorizontalAlignment = Element.ALIGN_LEFT;
            rej.Border = 0;
            dane.AddCell(rej);

            PdfPCell rok = new PdfPCell(new Phrase("Rok produkcji: " + RokProdukcji.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            rok.HorizontalAlignment = Element.ALIGN_LEFT;
            rok.Border = 0;
            dane.AddCell(rok);
            
            PdfPCell szkoda = new PdfPCell(new Phrase("Numer szkody: " + NumerSzkody.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            szkoda.HorizontalAlignment = Element.ALIGN_LEFT;
            szkoda.Border = 0;
            dane.AddCell(szkoda);

            PdfPCell tu = new PdfPCell(new Phrase("Towarzystwo ubezpieczeniowe: " + TowUb.Text, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tu.HorizontalAlignment = Element.ALIGN_LEFT;
            tu.Border = 0;
            dane.AddCell(tu);

            document.Add(dane);
            PdfPTable table = new PdfPTable(4);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            float[] widths = new float[] { 30f, 270f, 80f, 120f };

            table.SetWidths(widths);

            table.HorizontalAlignment = 0;
            table.SpacingBefore = 20f;
            
            table.SpacingAfter = 30f;
            PdfPCell czynn = new PdfPCell(new Phrase("Wykonana czynność", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            czynn.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell lp = new PdfPCell(new Phrase("L.p.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            lp.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell dat = new PdfPCell(new Phrase("Data", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            dat.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell podp = new PdfPCell(new Phrase("Podpis pracownika serwisu", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            podp.HorizontalAlignment = Element.ALIGN_CENTER;
            table.AddCell(lp);
            table.AddCell(czynn);
            table.AddCell(dat);
            table.AddCell(podp);
            

            PdfPCell one = new PdfPCell(new Phrase("Przyjęcie pojazdu do serwisu", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            one.MinimumHeight = 20f;
            table.AddCell("1.");
            table.AddCell(one);
            table.AddCell(GetDate.Text);
            table.AddCell("");

            PdfPCell two = new PdfPCell(new Phrase("Zgłoszenie szkody w T.U.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            two.MinimumHeight = 20f;
            table.AddCell("2.");
            table.AddCell(two);
            table.AddCell(ZgloszSzkWTU.Text);
            table.AddCell("");

            PdfPCell three = new PdfPCell(new Phrase("Oględziny przednaprawcze w T.U.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            three.MinimumHeight = 20f;
            table.AddCell("3.");
            table.AddCell(three);
            table.AddCell(OglPrzednWTU.Text);
            table.AddCell("");

            PdfPCell four = new PdfPCell(new Phrase("Dostarczenie do serwisu kosztorysu naprawy", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            four.MinimumHeight = 20f;
            table.AddCell("4.");
            table.AddCell(four);
            table.AddCell(DostDoSerwKosztNapr.Text);
            table.AddCell("");

            PdfPCell five = new PdfPCell(new Phrase("Wykonanie kalkulacji przez serwis", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            five.MinimumHeight = 20f;
            table.AddCell("5.");
            table.AddCell(five);
            table.AddCell(WykonKalkSerw.Text);
            table.AddCell("");

            PdfPCell six = new PdfPCell(new Phrase("Zatwierdzenie kalkulacji serwisu przez T.U.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            six.MinimumHeight = 20f;
            table.AddCell("6.");
            table.AddCell(six);
            table.AddCell(ZatwKalkPrzezTU.Text);
            table.AddCell("");

            PdfPCell sev = new PdfPCell(new Phrase("Rozpoczęcie naprawy pojazdu klienta", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            sev.MinimumHeight = 20f;
            table.AddCell("7.");
            table.AddCell(sev);
            table.AddCell(RozpNapr.Text);
            table.AddCell("");

            PdfPCell eight = new PdfPCell(new Phrase("Zgłoszenie T.U. dodatkowych oględzin", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            eight.MinimumHeight = 20f;
            table.AddCell("8.");
            table.AddCell(eight);
            table.AddCell(ZglTUDodOgl.Text);
            table.AddCell("");

            PdfPCell nine = new PdfPCell(new Phrase("Oględziny dodatkowe wykonane przez T.U.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            nine.MinimumHeight = 20f;
            table.AddCell("9.");
            table.AddCell(nine);
            table.AddCell(OglDodWykTU.Text);
            table.AddCell("");

            PdfPCell ten = new PdfPCell(new Phrase("Zatwierdzenie kosztorysu po oględzinach dodatkowych", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            ten.MinimumHeight = 20f;
            table.AddCell("10.");
            table.AddCell(ten);
            table.AddCell(ZatwKosztOglDod.Text);
            table.AddCell("");

            PdfPCell eleven = new PdfPCell(new Phrase("Zamówienie części", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            eleven.MinimumHeight = 20f;
            table.AddCell("11.");
            table.AddCell(eleven);
            table.AddCell(ZamowCzesci.Text);
            table.AddCell("");

            PdfPCell twelve = new PdfPCell(new Phrase("Odbiór części", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            twelve.MinimumHeight = 20f;
            table.AddCell("12.");
            table.AddCell(twelve);
            table.AddCell(OdbCzesci.Text);
            table.AddCell("");

            PdfPCell thirteen = new PdfPCell(new Phrase("Zakończenie naprawy pojazdu klienta", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            thirteen.MinimumHeight = 20f;
            table.AddCell("13.");
            table.AddCell(thirteen);
            table.AddCell(ZakonNapr.Text);
            table.AddCell("");

            PdfPCell fourteen = new PdfPCell(new Phrase("Odbiór pojazdu przez klienta", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            fourteen.MinimumHeight = 20f;
            table.AddCell("14.");
            table.AddCell(fourteen);
            table.AddCell(OdbiorPoj.Text);
            table.AddCell("");

            document.Add(table);










            Paragraph uwagi = new Paragraph();
            uwagi.Alignment = Element.ALIGN_LEFT;
            uwagi.Add(new Chunk("Uwagi: ",
                new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(uwagi);

            Paragraph uwagi2 = new Paragraph();
            uwagi2.SpacingBefore = 10;
            uwagi2.Alignment = Element.ALIGN_LEFT;
            uwagi2.Add(new Chunk(  Uwagi.Text ,
                new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(uwagi2);



            document.Close();
            writer.Close();
            fs.Close();

        }
        private void ButtonPrintPressed(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }

        private void ButtonPrinterPressed(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            printDialog.PageRangeSelection = PageRangeSelection.AllPages;
            printDialog.UserPageRangeEnabled = true;
            bool? doPrint = printDialog.ShowDialog();
            if (doPrint != true)
            {
                return;
            }
        }
    }
}
