﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for UmowaCesjiPage.xaml
    /// </summary>
    public partial class UmowaCesjiPage : UserControl
    {
        public UmowaCesjiPage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(UmowaCesji_Loaded);
        }
        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
           
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }
            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }

        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Umowa cesji ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 20;
            document.Add(prgHeader);

            Paragraph tresc = new Paragraph();
            tresc.Alignment = Element.ALIGN_LEFT;
            tresc.Add(new Chunk("Zawarta dnia " + data_zaw.Text.ToString() + " w " + miejsce_cesji.Text.ToString() + " pomiędzy: ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc.SpacingAfter = 5;
            document.Add(tresc);

            Paragraph tresc2 = new Paragraph();
            tresc2.Alignment = Element.ALIGN_LEFT;
            tresc2.Add(new Chunk("Shine of Divine Sp. z o.o. , ul. Lisia 20b, 05-270 Marki, zwanym dalej Cesjonariuszem ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc2.SpacingAfter = 5;
            document.Add(tresc2);

            Paragraph tresc3 = new Paragraph();
            tresc3.Alignment = Element.ALIGN_LEFT;
            tresc3.Add(new Chunk("a " +imie.Text.ToString()+" "+ miasto.Text.ToString()+" "+ulica.Text.ToString() +" legitymujacym się nr PESEL: "+
                pesel.Text.ToString()+", zwanym dalej Cedentem. ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc3.SpacingAfter = 20;
            document.Add(tresc3);

            Paragraph tresc4 = new Paragraph();
            tresc4.Alignment = Element.ALIGN_LEFT;
            tresc4.Add(new Chunk("1) Przedmiotem niniejszej umowy jest cesja wierzytelności, przysługującej Cedentowi z tytułu szkody z dnia "+dzien_szkody.Text.ToString()+" powstałej w "+miejsce_szkody.Text.ToString()+
                ", gdzie uszkodzeniu uległ jego pojazd mark "+samochod.Text.ToString()+ " nr rej. "+nr_rej.Text.ToString()+" z winy kierującego pojazdem marki "+sam_spr.Text.ToString()+
                " nr rej. " + nr_rej_spr.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc4.SpacingAfter = 5;
            document.Add(tresc4);

            Paragraph tresc5 = new Paragraph();
            tresc5.Alignment = Element.ALIGN_LEFT;
            tresc5.Add(new Chunk("2) Cedent przenosi niniejszym na Cesjonariusza swoją wierzytelność z tytułu szkody określonej w pkt 1 § 1 wobec dłużników, tj.sprawcy szkody " +
                imiespr.Text.ToString()+" oraz jego ubezpieczyciela " +tusprawcy.Text.ToString()+" nr szkody " +nr_szkody.Text.ToString()+
                ", w części obejmującej zwrot kosztów odholowania pojazdu Cedenta z parkingu firmy Shine of Divine Sp.z o.o. , ul.Lisia 20b, 05 - 270 Marki do " +
                odholowoano.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc5.SpacingAfter = 5;
            document.Add(tresc5);

            Paragraph tresc6 = new Paragraph();
            tresc6.Alignment = Element.ALIGN_LEFT;
            tresc6.Add(new Chunk("3) Przez koszt holowania rozumie się wszelkie czynności podjęte celem skutecznego przetransportowania pojazdu Cedenta do miejsca docelowego(m.in. koszt dojazdu pojazdu holującego na miejsce postoju uszkodzonego pojazdu, wyciąganie z rowu, załadunek, zabezpieczenie i czynności przygotowawcze, przewóz, rozładunek, powrót pojazdu holującego z miejsca docelowego).  ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc6.SpacingAfter = 5;
            document.Add(tresc6);

            Paragraph tresc7 = new Paragraph();
            tresc7.Alignment = Element.ALIGN_LEFT;
            tresc7.Add(new Chunk("4) Wartość przenoszonej wierzytelności zostanie potwierdzona w fakturze VAT, wystawionej przez Cesjonariusza na Cedenta.  ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc7.SpacingAfter = 20;
            document.Add(tresc7);

            Paragraph tresc8 = new Paragraph();
            tresc8.Alignment = Element.ALIGN_LEFT;
            tresc8.Add(new Chunk("Podstawą roszczenia Cedenta stanowi policyjna notatka z miejsca zdarzenia lub oświadczenie sprawcy, w którym przyznaje się do winy za spowodowanie wypadku / kolizji drogowej.  ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc8.SpacingAfter = 5;
            document.Add(tresc8);

            Paragraph tresc9 = new Paragraph();
            tresc9.Alignment = Element.ALIGN_LEFT;
            tresc9.Add(new Chunk("W przypadku zapłaty Cedentowi odszkodowania w części obejmującej zwrot kosztów holowania samochodu zamiast na rzecz Cesjonariusza, Cedent zobowiązuje się do niezwłocznego przelania otrzymanej kwoty na rachunek Cesjonariusza.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc9.SpacingAfter = 5;
            document.Add(tresc9);

            Paragraph tresc10 = new Paragraph();
            tresc10.Alignment = Element.ALIGN_LEFT;
            tresc10.Add(new Chunk("W sprawach nieuregulowanych w niniejszej Umowie mają zastosowanie przepisy Kodeksu cywilnego.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc10.SpacingAfter = 5;
            document.Add(tresc10);

            Paragraph tresc11 = new Paragraph();
            tresc11.Alignment = Element.ALIGN_LEFT;
            tresc11.Add(new Chunk("Niniejsza Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze Stron.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc11.SpacingAfter = 40;
            document.Add(tresc11);

            Paragraph tresc12 = new Paragraph();
            tresc12.Alignment = Element.ALIGN_LEFT;
            tresc12.Add(new Chunk("..............................                                                                                        ...............................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            //tresc12.SpacingAfter = 20;
            document.Add(tresc12);

            Paragraph tresc13 = new Paragraph();
            tresc13.Alignment = Element.ALIGN_LEFT;
            tresc13.Add(new Chunk("    Cesjonariusz                                                                                                       Cedent", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            //tresc12.SpacingAfter = 20;
            document.Add(tresc13);

            document.Close();
            writer.Close();
            fs.Close();
        }

            private void Upow_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }

        void UmowaCesji_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }
    }
}
