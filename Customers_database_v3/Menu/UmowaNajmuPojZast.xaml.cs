﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for UmowaNajmuPojZast.xaml
    /// </summary>
    public partial class UmowaNajmuPojZast : UserControl
    {
        public UmowaNajmuPojZast()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Umowanajmupojzast_Loaded);

        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }

            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            
        }
            private void Add_button(object sender, RoutedEventArgs e)
            {
                Switcher.Switch(new AddPage());

            }

            private void ShowCust_button(object sender, RoutedEventArgs e)
            {
                Switcher.Switch(new ShowCustomersPage());
            }



            private void Home_button(object sender, RoutedEventArgs e)
            {
                Switcher.Switch(new HomePage());
            }

            private void Cesje_button(object sender, RoutedEventArgs e)
            {
                Switcher.Switch(new CesjaPage());
            }
            void Umowanajmupojzast_Loaded(object sender, RoutedEventArgs e)
            {
                Window w = Window.GetWindow(MenuBar);
                if (null != w)
                {
                    w.LocationChanged += delegate (object sender2, EventArgs args)
                    {
                        var offset = MenuBar.HorizontalOffset;
                        MenuBar.HorizontalOffset = offset + 1;
                        MenuBar.HorizontalOffset = offset;
                    };
                    w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                    {
                        var offset = MenuBar.HorizontalOffset;
                        MenuBar.HorizontalOffset = offset + 1;
                        MenuBar.HorizontalOffset = offset;
                    };
                }
            }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph header = new Paragraph();
            header.Alignment = Element.ALIGN_CENTER;
            header.Add(new Chunk("UMOWA NAJMU POJAZDU ZASTĘPCZEGO CESJI WIERZYTELNOŚCI NR" + nr_umowy.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            header.SpacingAfter = 5;
            document.Add(header);

            Paragraph header2 = new Paragraph();
            header2.Alignment = Element.ALIGN_CENTER;
            header2.Add(new Chunk("zawarta w " + miej_umowy.Text.ToString()+" w dniu "+ data_umowy.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            header2.SpacingAfter = 10;
            document.Add(header2);

            PdfPTable dane = new PdfPTable(2);

            dane.TotalWidth = 500f;

            dane.LockedWidth = true;
            float[] widths1 = new float[] { 1f, 1f };
            dane.SetWidths(widths1);
            dane.HorizontalAlignment = 0;
            dane.SpacingBefore = 10f;
            dane.SpacingAfter = 5f;
            BaseFont bf = BaseFont.CreateFont(
                        @"C:\Windows\Fonts\Arial.ttf",
                        BaseFont.CP1250,
                        true);
            Font font = new Font(bf, 10);
            Font fontsmall = new Font(bf, 7);


            Font fdefault = FontFactory.GetFont("Arial", 10, Font.NORMAL, BaseColor.RED);

            

            PdfPCell daneshine = new PdfPCell(new Phrase("I. Dane Wynajmującego: \n \n Shine of Divine Sp. z o.o. \n 05 - 270 Marki \n ul.Lisia 20b \n NIP: 125 163 77 12 " , fdefault) );
            daneshine.HorizontalAlignment = Element.ALIGN_LEFT;
            
            //tu.Border = 0;
            dane.AddCell(daneshine);

            PdfPCell danenajemcy = new PdfPCell(new Phrase("II. Dane Najemcy: \n \n1. Nazwa/imię i nazwisko: " +imie_naz.Text.ToString()+
                "\n2. Adres: "+adres.Text.ToString()+"\n3. KRS/EDG/PESEL: "+krsedgpesel.Text.ToString() +"\n4. NIP/REGON: "+nipregon.Text.ToString()+"\n5. Tel: "+
                nr_tel.Text.ToString()+"\n6. E-mail: " +email.Text.ToString()+"\n7.Pełnomocnik: "+pelnomocnik.Text.ToString(),font ));
            danenajemcy.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(danenajemcy);

            PdfPCell danepojzast = new PdfPCell(new Phrase("III. Dane pojazdu zastępczego: \n \n1. Marka:..........................................................\n2. Numer rejestracyjny:....................................\n3. Okres najmu:................................................\n4. Cena najmu:.................................................\nPodpis: ", font));
            danepojzast.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;

            dane.AddCell(danepojzast);
            PdfPCell daneuszkpoj = new PdfPCell(new Phrase("IV. Dane uszkodzonego pojazdu: \n \n1. Marka: "+samochod.Text.ToString()+"\n2. Numer rejestracyjny: "+nr_rej.Text.ToString()+"\n3. Rok produkcji: "+rok_prod.Text.ToString()+
                "\n4. Numer polisy OC: "+nrpolisyoc.Text.ToString()+"\n5. Towarzystwo ubezpieczeń: "+tu.Text.ToString(), font));
            daneuszkpoj.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(daneuszkpoj);

            
            PdfPCell wydzwr = new PdfPCell(new Phrase("V. Wydanie/zwrot pojadu zastępczego: \n\n1. Data wydania pojazdu:................................\nPodpis:.............................................................\n2. Data zwrotu pojazdu:...................................\nPodpis:............................................................." , font));
            wydzwr.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(wydzwr);

            string imie_t = "";
            string adres_t = "";
            if (takiesame.IsChecked == true)
            {
                imie_t = imie_naz.Text.ToString();
                adres_t = adres.Text.ToString();
            }
            if (takiesame.IsChecked == false)
            {
                imie_t = wl_nazwa.Text.ToString();
                adres_t = wl_adres.Text.ToString();
            }

            PdfPCell danedotwluszkpoj = new PdfPCell(new Phrase("VI. Dane właściciela uszkodzonego pojazdu:\n\n1. Nazwa/Imię i nazwisko: " + imie_t+"\n2. Adres: "+adres_t, font));
            danedotwluszkpoj.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(danedotwluszkpoj);

            PdfPCell danedotpojspr = new PdfPCell(new Phrase("VII. Dane dotyczące pojazdu sprawcy:\n\n1. Sprawca: " + spr_nazwa.Text.ToString() + "\n2. Marka pojazdu: " + spr_samochod.Text.ToString()+"\n3. Numer rejestracyjny: "+spr_nr_rej.Text.ToString()+"\n4. Numer polisy OC: "+spr_nr_polisy+"\n5. Towarzystwo ubezpieczeń: "+
                spr_tu.Text.ToString()+ "\n6. Właściciel pojazdu: " + spr_wl_nazwa.Text.ToString()+"\n7. Adres: "+spr_adres.Text.ToString(), font));
            danedotpojspr.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(danedotpojspr);

            PdfPCell danedotzdarz = new PdfPCell(new Phrase("VIII. Dane dotyczące zdarzenia\n \n1. Miejsce: "+miej_szkody.Text.ToString()+"\n2. Data: "+data_szkody.Text.ToString()+"\n\n3. Numer szkody:..........................................." , font));
            danedotzdarz.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(danedotzdarz);

            PdfPCell uwagi = new PdfPCell(new Phrase("IX. Uwagi\n\nW pojeździe obowiązuje zakaz palenia tytoniu, spożywania posiłków, przewozu\nzwierząt oraz materiałów budowlanych.\n\n.....................................................................", font));
            uwagi.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(uwagi);

            PdfPCell dodinfo = new PdfPCell(new Phrase("X. Dodatkowe informacje:\n\n1. Czy na miejscu zdarzenia była policja\n □ TAK □ NIE\n2. Czy została sporządzona notka\n □ TAK  □ NIE\n3. Czy Najemca jest płatnikiem podatku VAT\n □ TAK  □ NIE\n", font));
            dodinfo.HorizontalAlignment = Element.ALIGN_LEFT;
            //tu.Border = 0;
            dane.AddCell(dodinfo);

            PdfPCell jeden = new PdfPCell(new Phrase("Niniejszym zostaje zawarta umowa najmu pojazdu zastępczego pomiędzy Wynajmującym, aNajemcą określonymi w pkt I i II powyższej tabeli.\n                                                §1\nWynajmujący udostępnia Najemcy pojazd zastępczy na zasadach określonych w warunkach ogólnych Programu „Zastępczy”, które stanowią integralną część umowy.\n                                                §2\nDane pojazdu zastępczego, okres na jaki wynajmowany jest pojazd zastępczy oraz jego cena określone są w pkt.III powyższej tabeli.\n                                                §3\nWynajmujący oświadcza, iż przekazywane Najemcy auto zastępcze jest w dobrym stanie oraz objęte jest ochroną ubezpieczeniową od odpowiedzialności cywilnej poprzez umowę zawartą na podstawie odrębnych przepisów.\n                                                §4\n1. Najemca oświadcza, że nie przysługuje mu prawo najmu pojazdu zastępczego z tytułu zawartych umów ubezpieczenia, umów leasingu i z innych źródeł.\n2.Najemca oświadcza, że otrzymał Warunki ogólne Programu „Zastępczy”, zapoznał się z ich treścią i nie wnosi do nich żadnych zastrzeżeń.\n3.Najemca wyraża zgodę na przetwarzanie jego danych osobowych przez Wynajmującego (będącego administratorem zbioru danych osobowych), oraz zajmujący się dochodzeniem roszczeń z tytułu najmu pojazdów zastępczych tylko i wyłącznie w przedmiotowej sprawie, a także inne podmioty realizujące Program w zakresie związanym z uczestnictwem Najemcy w Programie, oraz że został poinformowany o prawie wglądu do treści jego danych i możliwości ich poprawienia\n                                                §5\n1. Najemca zobowiązuje się ponosić wszystkie koszty związane z mandatami karnymi oraz wszelkie inne opłaty obciążające Wynajmującego związane z popełnionymi podczas trwania najmu pojazdu wykroczeniami drogowymi.\n2.Jeżeli Najemca jest płatnikiem podatku VAT, Najemca zobowiązuje się do pokrycia kwoty podatku VAT wskazanego na fakturze za najem pojazdu zastępczego w terminie wskazanym na w / w fakturze. ", fontsmall));
            jeden.HorizontalAlignment = Element.ALIGN_LEFT;
            jeden.Border = 0;
            dane.AddCell(jeden);

            PdfPCell dwa = new PdfPCell(new Phrase("                                                §6\n1. Najemca (cedent) na warunkach określonych w niniejszej umowie przelewa na rzecz Wynajmującego(cesjonariusza) swoją wierzytelność – prawo do zwrotu kosztów z tytułu najmu pojazdu zastępczego przysługującą Najemcy wobec sprawcy oraz jego towarzystwa ubezpieczeń sprawcy szkody wskazanej w pkt VIII tabeli.Wraz z wierzytelnością przechodzą na Wynajmującego wszelkie związane z nią prawa, w szczególności roszczenia o zaległe odsetki.\n2.Cedent oświadcza, że w dniu podpisania Umowy nie posiada względem towarzystwa ubezpieczeń żadnych zobowiązań, które mogłyby być przedmiotem wzajemnych potrąceń.\n3.Cesjonariusz oświadcza, iż przyjmuje wierzytelność.\n                                                §7\n1. Wynajmujący ma prawo odstąpić od umowy cesji wierzytelności i domagać się zapłaty bezpośrednio od Najemcy, jeżeli Najemca:\na.przekazał Wynajmującemu w związku z niniejszą umową, informacje niezgodne ze stanem faktycznym - w szczególności dotyczące szkody komunikacyjnej, o której mowa w pkt VII i VIII tabeli,\nb.wbrew obowiązkom wynikającym z umowy, nie udziela Wynajmującemu informacji lub nie przekazuje dokumentów, które dotyczą wierzytelności.\n2.Wynajmujący może skorzystać z prawa odstąpienia w terminie do 21 dni od dnia, w którym powziął informację o którejkolwiek okoliczności wymienionej w §7.1.\n                                                §8\n1. Do spraw nieobjętych powyższymi zapisami umowy znajdują zastosowanie właściwe przypisy Kodeksu Cywilnego.\n2.Wszelkie spory dotyczące treści oraz realizacji umowy podlegają rozstrzygnięciu przez sąd według właściwości ogólnej.\n3.Wszelkie zmiany treści niniejszej umowy powinny nastąpić w formie pisemnej pod rygorem nieważności.\n                                                §9\nUmowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.", fontsmall));
            dwa.HorizontalAlignment = Element.ALIGN_LEFT;
            dwa.Border = 0;
            dane.AddCell(dwa);

            document.Add(dane);



            document.Close();
            writer.Close();
            fs.Close();
        }

        private void Osw_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }
    }
    }

