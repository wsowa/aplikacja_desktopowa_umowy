﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for OswiadczenieVatPage.xaml
    /// </summary>
    public partial class OswiadczenieVatPage : UserControl
    {
        public OswiadczenieVatPage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(OswiadczenieVatPage_Loaded);
        }
        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }

        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();
            Paragraph data = new Paragraph();
            data.Alignment = Element.ALIGN_RIGHT;
            data.Add(new Chunk("dnia " + data_wyd.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            data.SpacingAfter = 10;
            document.Add(data);

            Paragraph dane = new Paragraph();
            dane.Alignment = Element.ALIGN_LEFT;
            dane.Add(new Chunk(imienaz.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            //dane.SpacingAfter = 10;
            document.Add(dane);
            Paragraph dane2 = new Paragraph();
            dane2.Alignment = Element.ALIGN_LEFT;
            dane2.Add(new Chunk(adres.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            dane2.SpacingAfter = 50;
            document.Add(dane2);

            Paragraph header = new Paragraph();
            header.Alignment = Element.ALIGN_CENTER;
            header.Add(new Chunk("OŚWIADCZENIE", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            header.SpacingAfter = 30;
            document.Add(header);

            Paragraph tresc = new Paragraph();
            tresc.Alignment = Element.ALIGN_LEFT;
            tresc.Add(new Chunk("Niniejszym oświadczam że: ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc.SpacingAfter = 5;
            document.Add(tresc);
            string czy_prowadze = "";
            if (prowadze.IsChecked == true)
            {
                czy_prowadze = "Prowadzę działalność gospodarczą. ";
            }
          
                if (nie_prowadze.IsChecked == true)
                {
                    czy_prowadze = "Nie prowadzę działalności gospodarczej. ";
                }
                if(prowadze.IsChecked == true && nie_prowadze.IsChecked == true)
                {
                    czy_prowadze = " Prowadzę/nie prowadzę* działalność gospodarczą.";
                }
            
            
            Paragraph tresc2 = new Paragraph();
            tresc2.Alignment = Element.ALIGN_LEFT;

            tresc2.Add(new Chunk("1. "+czy_prowadze, new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc2.SpacingAfter = 5;
            document.Add(tresc2);

            string sr_trw = "";

            if (jest_sr_trwalym.IsChecked == true)
            {
                sr_trw = "jest ";
            }
            
                if (niejest_sr_trwalym.IsChecked == true)
                {
                    sr_trw = "nie jest ";
                }
               if(jest_sr_trwalym.IsChecked == true && niejest_sr_trwalym.IsChecked == true)
                {
                    sr_trw = " jest/ nie jest* ";
                }
            


            Paragraph tresc3 = new Paragraph();
            tresc3.Alignment = Element.ALIGN_LEFT;

            tresc3.Add(new Chunk("2. Pojazd marki " + samochod.Text.ToString() + " o nr rej. " + nr_rej.Text.ToString() +" "+ sr_trw +" środkiem trwałym wpisanym do ewidencji środków trwałych." , new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            tresc3.SpacingAfter = 5;
            document.Add(tresc3);

            string wykorz_w_dzial = "";

            if (jest_wykorz_w_dzialalnosci.IsChecked == true)
            {
                wykorz_w_dzial = "jest ";
            }
            
                if (niejest_wykorz_w_dzialalnosci.IsChecked == true)
                {
                    wykorz_w_dzial = "nie jest ";
                }
                if(jest_wykorz_w_dzialalnosci.IsChecked == true && niejest_wykorz_w_dzialalnosci.IsChecked == true)
                {
                    wykorz_w_dzial = " jest/ nie jest* ";
                }
            


            Paragraph tresc4 = new Paragraph();
            tresc4.Alignment = Element.ALIGN_LEFT;

            tresc4.Add(new Chunk("3. Pojazd marki " + samochod2.Text.ToString() + " o nr rej. " + nr_rej2.Text.ToString() + " " + wykorz_w_dzial + " pojazdem wykorzystywanym w prowadzonej działalności, dla którego prowadzona jest ewidencja przebiegu pojazdu.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            tresc4.SpacingAfter = 5;
            document.Add(tresc4);

            string podat_vat = "";

            if (jest_podat_vat.IsChecked == true)
            {
                podat_vat = "Jestem ";
            }
            
                if (niejest_podat_vat.IsChecked == true)
                {
                    podat_vat = "Nie jestem ";
                }
                if(jest_podat_vat.IsChecked == true && niejest_podat_vat.IsChecked == true)
                {
                    podat_vat = "Jestem/nie jestem* ";
                }
            

            Paragraph tresc5 = new Paragraph();
            tresc5.Alignment = Element.ALIGN_LEFT;

            tresc5.Add(new Chunk("4. " +podat_vat+ " podatnikiem podatku od towarów i usług VAT.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            tresc5.SpacingAfter = 5;
            document.Add(tresc5);

            Paragraph tresc6 = new Paragraph();
            tresc6.Alignment = Element.ALIGN_LEFT;

            tresc6.Add(new Chunk("5. NIP " + nip.Text.ToString()+".", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
             
            tresc6.SpacingAfter = 5;
            document.Add(tresc6);

            string przysl = "";

            if (przysl_pr_vat.IsChecked == true)
            {
                przysl = " przysługuje ";
            }
            
                if (nie_przysl_pr_vat.IsChecked == true)
                {
                    przysl = " nie przysługuje ";
                }
                if(przysl_pr_vat.IsChecked == true && nie_przysl_pr_vat.IsChecked == true)
                {
                    przysl = " przysługuje/nie przysługuje* ";
                }
            


            Paragraph tresc7 = new Paragraph();
            tresc7.Alignment = Element.ALIGN_LEFT;

            tresc7.Add(new Chunk("6. W związku z prowadzoną działalnościa gospodarczą" + przysl+ " mi prawo do odliczenia podatku VAT naliczonego w związku z rozliczeniem kosztów naprawy w/w pojazdu.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            tresc7.SpacingAfter = 50;
            document.Add(tresc7);


            Paragraph podpis = new Paragraph();
            podpis.Alignment = Element.ALIGN_RIGHT;

            podpis.Add(new Chunk(".................................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            
            document.Add(podpis);

            Paragraph podpis2 = new Paragraph();
            podpis2.Alignment = Element.ALIGN_RIGHT;

            podpis2.Add(new Chunk("            podpis            ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            
            document.Add(podpis2);


            document.Close();
            writer.Close();
            fs.Close();
        }

            private void Osw_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }

        void OswiadczenieVatPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }

    }
}
