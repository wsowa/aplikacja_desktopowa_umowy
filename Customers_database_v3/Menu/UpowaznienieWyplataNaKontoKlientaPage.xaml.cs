﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for UpowaznienieWyplataNaKontoKlientaPage.xaml
    /// </summary>
    public partial class UpowaznienieWyplataNaKontoKlientaPage : UserControl
    {
        public UpowaznienieWyplataNaKontoKlientaPage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(UpowkontroklPage_Loaded);
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }

            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }




        }



        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }

        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }
        void UpowkontroklPage_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Upoważnienie do wypłaty odszkodowania ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 50;
            document.Add(prgHeader);

            Paragraph tresc = new Paragraph();
            tresc.Alignment = Element.ALIGN_LEFT;
            tresc.Add(new Chunk("W związku ze szkodą nr "+nr_szkody.Text.ToString()+" z dnia " +dzien_szkody.Text.ToString()+ " likwidowaną w ramach ubezpieczenia (OC,AC) dotyczącą samochodu marki " +
                samochod.Text.ToString()+" nr rej. "+ nr_rej.Text.ToString()+ " upoważniam Towarzystwo Ubezpieczeniowe " + tu.Text.ToString()+" do wypłaty odszkodowania na rzecz:" , new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc.SpacingAfter = 5;
            document.Add(tresc);

            Paragraph tresc2 = new Paragraph();
            tresc2.Alignment = Element.ALIGN_LEFT;
            tresc2.Add(new Chunk(dane.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc2.SpacingAfter = 5;
            document.Add(tresc2);

            Paragraph tresc3 = new Paragraph();
            tresc3.Alignment = Element.ALIGN_LEFT;
            tresc3.Add(new Chunk("Numer rachunku bankowego: "+ nr_rach_bank.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            tresc3.SpacingAfter = 50;
            document.Add(tresc3);

            Paragraph podpis = new Paragraph();
            podpis.Alignment = Element.ALIGN_RIGHT;
            podpis.Add(new Chunk(".............................................................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(podpis);

            Paragraph podpis2 = new Paragraph();
            podpis2.Alignment = Element.ALIGN_RIGHT;
            podpis2.Add(new Chunk("(podpis właściciela/właścicieli pojazdu)", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(podpis2);

            document.Close();
            writer.Close();
            fs.Close();

        }

        private void Osw_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }
    }
}
