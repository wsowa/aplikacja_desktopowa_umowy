﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for CesjaUpowaznieniePage.xaml
    /// </summary>
    public partial class CesjaUpowaznieniePage : UserControl
    {
        public CesjaUpowaznieniePage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(CesjaUpowaznienie_Loaded);
        }
        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }
            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }
        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Upoważnienie ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 50;
            document.Add(prgHeader);

            Paragraph one = new Paragraph();
            one.Alignment = Element.ALIGN_LEFT;
            one.SpacingAfter = 5;
            one.Add(new Chunk("Ja, " + imie.Text.ToString() + " zamieszkały/a w " + miasto.Text.ToString() + " " + ulica.Text.ToString() + ",", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(one);

            Paragraph two = new Paragraph();
            two.Alignment = Element.ALIGN_LEFT;
            two.SpacingAfter = 5;
            two.Add(new Chunk("PESEL " + pesel.Text.ToString() + " upoważniam Dawida Abramka zamieszkałego ul. Lisia 20b, Marki", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(two);

            Paragraph three = new Paragraph();
            three.Alignment = Element.ALIGN_LEFT;
            three.SpacingAfter = 5;
            three.Add(new Chunk("PESEL 923756421273 do wszelkich czynności związanych z likwidacją szkody komunikacyjnej nr " +nr_szkody.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(three);

            Paragraph four = new Paragraph();
            four.Alignment = Element.ALIGN_LEFT;
            four.SpacingAfter = 5;
            four.Add(new Chunk("z dnia " + dzien_szkody.Text.ToString() + ", powstałej w miejscowości " + miejsce_szkody.Text.ToString() + " gdzie uszkodzeniu uległ mój pojazd marki " + samochod.Text.ToString() + " o nr rej. " + nr_rej.Text.ToString() + ".", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(four);

            //Paragraph five = new Paragraph();
            //five.Alignment = Element.ALIGN_LEFT;
            //five.Add(new Chunk(samochod.Text.ToString() + " o nr rej. " + nr_rej.Text.ToString() +".", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            //document.Add(five);

            Paragraph eight2 = new Paragraph();
            eight2.SpacingBefore = 30;
            eight2.Alignment = Element.ALIGN_LEFT;
            eight2.Add(new Chunk("                                                                                                                   .................................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            document.Add(eight2);
            Paragraph eight3 = new Paragraph();

            eight3.Alignment = Element.ALIGN_LEFT;
            eight3.Add(new Chunk("                                                                                                                           podpis", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            document.Add(eight3);

            document.Close();
            writer.Close();
            fs.Close();

        }
            private void Upow_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }
        void CesjaUpowaznienie_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }
    }
}
