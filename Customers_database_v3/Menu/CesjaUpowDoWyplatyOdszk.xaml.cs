﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for CesjaUpowDoWyplatyOdszk.xaml
    /// </summary>
    public partial class CesjaUpowDoWyplatyOdszk : UserControl
    {
        public CesjaUpowDoWyplatyOdszk()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(CesjaUpowDoWyplatyOdszk_Loaded);
        }

        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            if (ces == "Oświadczenie o potrzebie auta zastępczego")
            {
                Switcher.Switch(new CesjaOswiadczeniePage());
            }
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
           
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }
            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }
        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgdane = new Paragraph();
            prgdane.Alignment = Element.ALIGN_LEFT;
            prgdane.Add(new Chunk(imie.Text.ToString()+"                                                                                  "+miejsc.Text.ToString()+", dnia "+ data.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgdane.SpacingAfter = 5;
            document.Add(prgdane);

            Paragraph prgdane2 = new Paragraph();
            prgdane2.Alignment = Element.ALIGN_LEFT;
            prgdane2.Add(new Chunk(miasto.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(prgdane2);

            Paragraph prgdane3 = new Paragraph();
            prgdane3.Alignment = Element.ALIGN_LEFT;
            prgdane3.SpacingAfter = 40;
            prgdane3.Add(new Chunk(ulica.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(prgdane3);


            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Upoważnienie do wypłaty odszkodowania ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 20;
            document.Add(prgHeader);

            Paragraph tresc = new Paragraph();
            tresc.Alignment = Element.ALIGN_LEFT;
            tresc.SpacingAfter = 10;
            tresc.Add(new Chunk("W związku ze szkodą nr "+ nr_szkody.Text.ToString() + " z dnia " + data_szkody.Text.ToString() + " likwidowaną w ramach ubezpieczenia (OC, AC) dotycząca samochodu marki "
                + samochod.Text.ToString()+ " nr rej. " + nr_rej.Text.ToString() + " upoważniam Towarzystwo Ubezpieczeń "+ tow_ub.Text.ToString()+" do wypłaty odszkodowania wraz z kosztami dodatkowymi.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc);

            Paragraph tresc2 = new Paragraph();
            tresc2.Alignment = Element.ALIGN_LEFT;
            //tresc.SpacingAfter = 20;
            tresc2.Add(new Chunk("Na rzecz:", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc2);

            Paragraph tresc3 = new Paragraph();
            tresc3.Alignment = Element.ALIGN_LEFT;
            //tresc.SpacingAfter = 20;
            tresc3.Add(new Chunk("Shine of Divine Sp. z o.o.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc3);

            Paragraph tresc4 = new Paragraph();
            tresc4.Alignment = Element.ALIGN_LEFT;
            //tresc.SpacingAfter = 20;
            tresc4.Add(new Chunk("05-270 Marki", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc4);

            Paragraph tresc5 = new Paragraph();
            tresc5.Alignment = Element.ALIGN_LEFT;
            tresc5.SpacingAfter = 5;
            tresc5.Add(new Chunk("ul. Lisia 20b", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc5);

            Paragraph tresc6 = new Paragraph();
            tresc6.Alignment = Element.ALIGN_LEFT;
            tresc6.SpacingAfter = 5;
            tresc6.Add(new Chunk("Numer rachunku bankowego: 71 1140 2004 0000 3102 7603 1160", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc6);

            Paragraph tresc7 = new Paragraph();
            tresc7.Alignment = Element.ALIGN_LEFT;
            tresc7.SpacingAfter = 50;
            tresc7.Add(new Chunk("należnego mi od Państwa odszkodowania w ramach wyżej wymienionej umowy ubezpieczenia.", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc7);

            Paragraph tresc8 = new Paragraph();
            tresc8.Alignment = Element.ALIGN_LEFT;
            
            tresc8.Add(new Chunk(".........................................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc8);

            Paragraph tresc9 = new Paragraph();
            tresc9.Alignment = Element.ALIGN_LEFT;

            tresc9.Add(new Chunk("(podpis właściciela pojazdu)", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(tresc9);





            document.Close();
            writer.Close();
            fs.Close();
        }

            private void Upow_zapisz(object sender, RoutedEventArgs e)
        {
            
                DataTable dtbl = MakeDataTable();
                var openFileDialog = new Microsoft.Win32.SaveFileDialog
                {
                    DefaultExt = ".pdf",
                    Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
                };
                var fileOpenResult = openFileDialog.ShowDialog();
                ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
                if (fileOpenResult != true)
                {
                    return;
                }
            
        }

        void CesjaUpowDoWyplatyOdszk_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }
    }
}
