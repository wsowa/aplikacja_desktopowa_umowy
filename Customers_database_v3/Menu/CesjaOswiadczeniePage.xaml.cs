﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;

namespace Customers_database_v3.Menu
{
    /// <summary>
    /// Interaction logic for CesjaOswiadczeniePage.xaml
    /// </summary>
    public partial class CesjaOswiadczeniePage : UserControl
    {
        public CesjaOswiadczeniePage()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(CesjaOswiadczenie_Loaded);
        }
       


        private void Add_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new AddPage());

        }

        private void ShowCust_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new ShowCustomersPage());
        }



        private void Home_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new HomePage());
        }
        private void Cesje_button(object sender, RoutedEventArgs e)
        {
            Switcher.Switch(new CesjaPage());
        }

        private void Choose(object sender, RoutedEventArgs e)
        {
            
            string ces = CesjaChoose.SelectionBoxItem.ToString();
            
            if (ces == "Upoważnienie na likwidacje przy współwłasności")
            {
                Switcher.Switch(new CesjaUpowaznieniePage());
            }
            if (ces == "Upoważnienie do wypłaty odszkodowania")
            {
                Switcher.Switch(new CesjaUpowDoWyplatyOdszk());
            }
            if (ces == "Cesja na odholowanie przy szkodzie całkowitej")
            {
                Switcher.Switch(new UmowaCesjiPage());
            }
            if (ces == "Oświadczenie Vat")
            {
                Switcher.Switch(new OswiadczenieVatPage());
            }
            if (ces == "Upoważnienie na likwidacje i zgłoszenie szkody z AC")
            {
                Switcher.Switch(new UpowaznienieLikwSzkodyACPage());
            }

            if (ces == "Upoważnienie na wypłatę odszkodowania na konto klienta")
            {
                Switcher.Switch(new UpowaznienieWyplataNaKontoKlientaPage());
            }
            if (ces == "Cesja na holowanie i parking")
            {
                Switcher.Switch(new CesjaHolowanieParking());
            }
            if (ces == "Umowa najmu pojazdu zastępczego")
            {
                Switcher.Switch(new UmowaNajmuPojZast());
            }
        }

        DataTable MakeDataTable()
        {
            DataTable inv = new DataTable();
            inv.Columns.Add("Imie: ");
            inv.Columns.Add("Nazwisko: ");
            inv.Columns.Add("Adres: ");

            inv.Rows.Add("Adam", "Jakis", "Ulica 12 42-100 Klobuck ");

            return inv;
        }



        void ExportDataTableToPdf(DataTable dtblTable, String strPdfPath, string strHeader)
        {
            FileStream fs = new FileStream(strPdfPath, FileMode.Create, FileAccess.Write, FileShare.None);
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(document, fs);
            document.Open();

            Paragraph prgHeader = new Paragraph();
            prgHeader.Alignment = Element.ALIGN_CENTER;
            prgHeader.Add(new Chunk("Oświadczenie ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            prgHeader.SpacingAfter = 50;
            document.Add(prgHeader);

            Paragraph one = new Paragraph();
            one.Alignment = Element.ALIGN_LEFT;
            one.Add(new Chunk("Ja, " + imie.Text.ToString() + " zamieszkały/a w " + miasto.Text.ToString() + ", przy ul. " + ulica.Text.ToString() + ",", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(one);

            //Paragraph two = new Paragraph();
            //two.Alignment = Element.ALIGN_LEFT;
            //two.Add(new Chunk("zamieszkały/a w " + miasto.Text.ToString() + ", przy ul. " + ulica.Text.ToString() + ",", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            //document.Add(two);

            string kto = "";
            if (wlasciciel.IsChecked == true)
            {
                kto = " właścicielem ";
            }
            
                if (uzytkownik.IsChecked == true)
                {
                    kto = " użytkownikiem ";
                }
                if(wlasciciel.IsChecked == true && uzytkownik.IsChecked == true)
                { 
                    kto = " właścicielem/użytkownikiem ";
                }
            

            Paragraph three = new Paragraph();
            three.Alignment = Element.ALIGN_LEFT;
            three.Add(new Chunk("PESEL " + pesel.Text.ToString() +", będący/a"+kto , new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(three);

            Paragraph four = new Paragraph();
            four.Alignment = Element.ALIGN_LEFT;
            four.Add(new Chunk("uszkodzonego pojazdu " +samochod.Text.ToString()+", o numerze rejestracyjnym "+nr_rej.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(four);

            Paragraph five = new Paragraph();
            five.SpacingBefore = 20;
            five.Alignment = Element.ALIGN_LEFT;
            five.Add(new Chunk("oświadczam, iż nie mam możliwości zastąpienia uszkodzonego pojazdu innym pojazdem.",
                 new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(five);

            Paragraph five2 = new Paragraph();
            
            five2.Alignment = Element.ALIGN_LEFT;
            five2.Add(new Chunk(
                "W szczególności, nie przysługuje mi prawo najmu pojazdu zastępczego z tytułu zawartych umów ubezpieczenia, umów leasingu i z innych źródeł ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(five2);

            Paragraph six = new Paragraph();
            six.SpacingBefore = 20;
            six.Alignment = Element.ALIGN_LEFT;
            six.Add(new Chunk("Uszkodzony pojazd wykorzystywany jest do codziennego funkcjonowania, a pozbawienie mnie możliwości korzystania z niego powoduje konieczność dokonania najmu pojazdu zastępczego. ", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(six);

            Paragraph seven = new Paragraph();
            seven.SpacingBefore = 20;
            seven.Alignment = Element.ALIGN_LEFT;
            seven.Add(new Chunk("W związku z powyższym, oświadczam, iż samochód zastępczy jest mi niezbędny do:", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(seven);

            Paragraph eight = new Paragraph();
            eight.SpacingBefore = 20;
            eight.Alignment = Element.ALIGN_LEFT;
            eight.Add(new Chunk(osw.Text.ToString(), new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));
            document.Add(eight);

            Paragraph eight2 = new Paragraph();
            eight2.SpacingBefore = 30;
            eight2.Alignment = Element.ALIGN_LEFT;
            eight2.Add(new Chunk("...................dnia...................                                                                       .................................", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            document.Add(eight2);
            Paragraph eight3 = new Paragraph();
           
            eight3.Alignment = Element.ALIGN_LEFT;
            eight3.Add(new Chunk("                                                                                                                           podpis(y)", new Font(BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.CP1250, true))));

            document.Add(eight3);

            document.Close();
            writer.Close();
            fs.Close();

        }

        private void Osw_zapisz(object sender, RoutedEventArgs e)
        {
            DataTable dtbl = MakeDataTable();
            var openFileDialog = new Microsoft.Win32.SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF files (.pdf)|.pdf|All files (.)|*.*"
            };
            var fileOpenResult = openFileDialog.ShowDialog();
            ExportDataTableToPdf(dtbl, openFileDialog.FileName, "Faktura");
            if (fileOpenResult != true)
            {
                return;
            }
        }

        void CesjaOswiadczenie_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(MenuBar);
            if (null != w)
            {
                w.LocationChanged += delegate (object sender2, EventArgs args)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
                w.SizeChanged += delegate (object sender3, SizeChangedEventArgs e2)
                {
                    var offset = MenuBar.HorizontalOffset;
                    MenuBar.HorizontalOffset = offset + 1;
                    MenuBar.HorizontalOffset = offset;
                };
            }
        }
    }
}
